#!/usr/bin/env python
from sys import argv
from glob import glob
from pandas import read_csv
from numpy import float32, float64
from re import sub

def main(argv):
    for raw_macs in glob("data/macs/*.narrowPeak"):
        raw_df = read_csv(raw_macs, sep="\t", header=None)
        for col in raw_df.columns:
            if raw_df[col].dtype == float64:
                raw_df[col] = raw_df[col].round().astype(int)
        macs = sub(r'macs', "macs-corrected", raw_macs)
        raw_df.to_csv(macs, sep="\t", header=False, index=False)
    return 0

if __name__ == "__main__":
    returncode = main(argv)
    exit(returncode)
