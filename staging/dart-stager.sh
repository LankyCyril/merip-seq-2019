#!/bin/sh

refpref="data/reference/mm10"

for filename in $(find -L . -type f | grep -E '8012.+fastq\.gz'); do
    outpref=$(echo $filename | sed -E 's/reads/alignments/g; s/\.fastq\.gz//g')
    echo "tools/dart/dart -t 16 -i $refpref -f $filename -j ${outpref}.junc | samtools view -bh > ${outpref}.bam"
done
