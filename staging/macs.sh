macs2 callpeak -c data/alignments/2Hct-ct.bam -t data/alignments/2Hct-IP.bam --nomodel --extsize 100 -g 2600000000 -n data/macs/2Hct -f BAM &
macs2 callpeak -c data/alignments/4KO-ct.bam -t data/alignments/4KO-IP.bam --nomodel --extsize 100 -g 2600000000 -n data/macs/4KO -f BAM    &
macs2 callpeak -c data/alignments/6WT-ct.bam -t data/alignments/6WT-IP.bam --nomodel --extsize 100 -g 2600000000 -n data/macs/6WT -f BAM
