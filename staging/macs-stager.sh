#!/bin/sh

for filename in data/alignments/*-ct.bam; do
    experiment=$(basename $filename | cut -f1 -d'-')
    echo "macs2 callpeak -c $filename -t data/alignments/${experiment}-IP.bam --nomodel --extsize 100 -g 2600000000 -n data/macs/${experiment} -f BAM"
done
