#!/bin/sh

for filename in $(find -L . -type f | grep -E 'bam$'); do
    case $filename in
        *IP*) is_ip="IP" ;;
        *) is_ip="ct" ;;
    esac
    experiment=$(echo $filename | cut -f2 -d'_')
    echo "samtools sort -@ 16 -O BAM -o data/alignments/${experiment}-${is_ip}.bam $filename"
done
