#!/usr/bin/env Rscript

################################################################################
# The design matrix has the same number of samples and coefficients to fit,    #
# estimating dispersion by treating samples as replicates. This analysis       #
# is not useful for accurate differential expression analysis, and arguably    #
# not for data exploration either, as large differences appear as high         #
# dispersion.                                                                  #
################################################################################

source("hands-on-deq.r")

# reload S4Vectors after all DEQ-related prep (?):
library(zeallot)
library(S4Vectors)
library(DESeq2)
library(edgeR)

# input data, metadata, and info:
peak.files = c(
    "data/macs-corrected/6WT_peaks.narrowPeak",
    "data/macs-corrected/4KO_peaks.narrowPeak",
    "data/macs-corrected/2Hct_peaks.narrowPeak"
)
gtf = "data/reference/mm10.gtf"
paired.end = F
readlen = 100
fraglen = 100
extension = fraglen - readlen

# metadata defines Genotype as an ordered factor:
meta.data = read.csv("shared/annotation.tsv", sep="\t")
meta.data$Genotype = factor(
    meta.data$Genotype, ordered=TRUE, levels=c("wt", "het", "ko")
)
rownames(meta.data) = unlist(lapply(
    meta.data$Sample, function(s) paste("data/alignments/", s, sep="")
))
model.matrix.full = model.matrix(
    ~meta.data$Genotype+meta.data$IP+meta.data$Genotype*meta.data$IP
)
model.matrix.reduced = model.matrix(~meta.data$Genotype+meta.data$IP)

# without replicates, need to 'blind' the matrix just for the dispersion
# estimation step, a la the QNB 'blind' mode.
# setting Genotype temporarily to a numeric value is enough for this;
# it even retains *some* biological coherence, although it still only should
# be used for the single step of dispersion estimation:
meta.data.blinder = meta.data
meta.data.blinder$Genotype = as.numeric(meta.data.blinder$Genotype) - 1
model.matrix.blinder = model.matrix(
    ~meta.data.blinder$Genotype+meta.data.blinder$IP+meta.data.blinder$Genotype*meta.data.blinder$IP
)

# load the rest of data:
genenames = load_genenames(gtf)
anno = load_anno(gtf, genenames)
peaks = import.peaks(peak.files, anno)
peaks = count.reads(peaks, rownames(meta.data), paired.end, extension)
peak.counts = counts(peaks$peak.counts)
colnames(peak.counts) = rownames(meta.data)

# use the actual matrix to construct the DESeq2 dataset
# (where Genotype is an ordered factor with 'ko', 'het', and 'wt'):
inf.dds = DESeqDataSetFromMatrix(
    countData=peak.counts, colData=meta.data, design=model.matrix.full
)
inf.dds.ESF = estimateSizeFactors(inf.dds)

# substitute the model matrix to the 'blind' one temporarily, to calculate
# presumable dispersion:
inf.dds.ESF = estimateDispersionsGeneEst(
    inf.dds.ESF, modelMatrix=model.matrix.blinder
)
inf.dds.ESF = estimateDispersionsFit(inf.dds.ESF, fitType="parametric")
inf.dds.ESF = estimateDispersionsMAP(
    inf.dds.ESF, modelMatrix=model.matrix.blinder
)

# revert to actual realistic matrices for the LRT:
inf.dds.LRT = nbinomLRT(inf.dds.ESF, reduced=model.matrix.reduced)
peak.results.LRT = as.data.frame(results(
    inf.dds.LRT,
    contrast=list(c("meta.data.Genotype.Q"), c("meta.data.Genotype.L"))
))
colnames(peak.results.LRT) = unlist(lapply(
    colnames(peak.results.LRT), function (n) paste("peak-LRT", n, sep=".")
))

# now rinse and repeat with edgeR:
er.dgelist = DGEList(counts=peak.counts, group=meta.data$Genotype)
er.dgelist = estimateGLMCommonDisp(
    er.dgelist, design=NULL, method="deviance", verbose=T, robust=T, subset=NULL
)
er.fit = glmFit(er.dgelist, model.matrix.full)
er.lrt = glmLRT(er.fit, coef=4)
peak.results.edgeR = er.lrt$table
peak.results.edgeR$padj = p.adjust(peak.results.edgeR$PValue, method="BH")
colnames(peak.results.edgeR) = unlist(lapply(
    colnames(peak.results.edgeR), function (n) paste("peak-edgeR", n, sep=".")
))

# per-gene DESeq2:
sub.meta.data.blinder = meta.data.blinder[which(meta.data.blinder$IP==FALSE),]

peaks$gene.counts = get.gene.counts(
    rownames(sub.meta.data.blinder),
    gtf, paired.end, extension, genenames
)
sane_gene.counts = peaks$gene.counts
rownames(sane_gene.counts) = make.names(rownames(sane_gene.counts))
colnames(sane_gene.counts) = rownames(sub.meta.data.blinder)
gene.dds = DESeqDataSetFromMatrix(
    countData=sane_gene.counts, colData=sub.meta.data.blinder, design=~Genotype
)
gene.col2check = sub.meta.data.blinder$Genotype
gene.dds.Wald = DESeq(gene.dds, test="Wald")
gene.results.Wald = results(gene.dds.Wald, contrast=list(c("Genotype")))
colnames(gene.results.Wald) = unlist(lapply(
    colnames(gene.results.Wald), function (n) paste("Wald", n, sep=".")
))
gene.dds.LRT = DESeq(gene.dds, test="LRT", reduced=~1)
gene.results.LRT = results(gene.dds.LRT, contrast=list(c("Genotype")))
colnames(gene.results.LRT) = unlist(lapply(
    colnames(gene.results.LRT), function (n) paste("LRT", n, sep=".")
))

# per-gene edgeR:
sub.meta.data = meta.data[which(meta.data$IP==FALSE),]
er.gene.dgelist = DGEList(counts=sane_gene.counts, group=sub.meta.data$Genotype)
er.gene.dgelist = estimateGLMCommonDisp(
    er.gene.dgelist, design=NULL, method="deviance",
    verbose=T, robust=T, subset=NULL
)
er.gene.fit = glmFit(er.gene.dgelist, model.matrix(~sub.meta.data$Genotype))
er.gene.lrt = glmLRT(er.gene.fit, coef=3)
gene.results.edgeR = er.gene.lrt$table
gene.results.edgeR$padj = p.adjust(gene.results.edgeR$PValue, method="BH")
colnames(gene.results.edgeR) = unlist(lapply(
    colnames(gene.results.edgeR), function (n) paste("edgeR", n, sep=".")
))

# report!
peak.report = cbind(peaks$peaks, peak.results.LRT)
peak.report = cbind(peak.report, peak.results.edgeR)
write.table(
    peak.report, file="data/hands-on-deq/custom-peaks.tsv",
    sep="\t", quote=F
)
gene.report = cbind(gene.results.LRT, gene.results.Wald)
gene.report = cbind(gene.report, gene.results.edgeR)
write.table(
    gene.report, file="data/hands-on-deq/custom-genes.tsv",
    sep="\t", quote=F
)
