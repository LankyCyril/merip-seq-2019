#!/usr/bin/env Rscript

library(dplyr)
library(S4Vectors)
library(zeallot)

source("tools/deq/R/metdiff_function_copy.R")

count_bams = function(input.bams, ip.bams)
### return number of samples, fail if number is inconsistent between regular and IP ###
{
    n = length(input.bams)
    if (n != length(ip.bams))
        stop("number of IP bam files must equal number of input bam files")
    return(n)
}


make_metadata = function(n.c, n.t)
### generate generic sample annotation: control/treatment, input/IP ###
{
    return(data.frame(
        Condition=c(
            rep("control", n.c*2),
            rep("treatment", n.t*2)
        ),
        IP=c(
            rep("input", n.c), rep("IP", n.c),
            rep("input", n.t), rep("IP", n.t)
        )
    ))
}


load_genenames = function(gtf)
### read gene names from GTF ###
{
    gtf.in = rtracklayer::import(gtf)
    return(unique(as.data.frame(gtf.in)[, c("gene_id", "gene_name", "strand")]))
}


load_anno = function(gtf, genenames)
### read annotation from GTF ###
{
    txdb = GenomicFeatures::makeTxDbFromGFF(gtf, format="gtf")
    return(list(txdb=txdb, genenames=genenames))
}


findOverlapsFeature = function(gr, features, gene.ids, feature.name)
### adapted from CLIPanalyze ###
{
    hits = GenomicRanges::findOverlaps(gr, features)
    hits = data.table::as.data.table(hits)
    hits$tx_name = names(features)[hits$subjectHits]
    hits = merge(hits, gene.ids, by = "tx_name")
    hits = hits[, c("queryHits", "gene_name")] %>% unique(by=NULL)
    hits.arbitrary = hits %>% unique(by="queryHits")
    hits$name = gr[hits$queryHits]$name
    hits = hits[, c("name", "gene_name")] %>% unique(by=NULL)
    mcols(gr)[, feature.name] = as.character(NA)
    mcols(gr)[hits.arbitrary$queryHits, feature.name] = hits.arbitrary$gene_name
    return(list(gr=gr, hits=hits))
}


annotateFeatures = function(gr, txdb, genenames, utr5.extend=2000, utr3.extend=2000)
### adapted from CLIPanalyze ###
{
    gr$name = names(gr)
    message("prepare transcript and gene names... ")
    tx.names = AnnotationDbi::keys(txdb, "TXNAME")
    gene.ids = AnnotationDbi::select(
        txdb, keys=tx.names, columns="GENEID", keytype="TXNAME"
    )
    colnames(gene.ids) = c("tx_name", "gene_id")
    gene.ids = data.table::as.data.table(gene.ids)
    genenames = data.table::as.data.table(genenames)
    gene.ids = merge(gene.ids, genenames, by="gene_id")
    all.overlaps = list()
    message("annotate with exons... ")
    exons = GenomicFeatures::exonsBy(txdb, use.names=T)
    gr.hits = findOverlapsFeature(gr, exons, gene.ids, "exon")
    gr = gr.hits[["gr"]]
    all.overlaps = c(all.overlaps, list(exon=gr.hits[["hits"]]))
    message("annotate with introns... ")
    introns = GenomicFeatures::intronsByTranscript(txdb, use.names=T)
    gr.hits = findOverlapsFeature(gr, introns, gene.ids, "intron")
    gr = gr.hits[["gr"]]
    all.overlaps = c(all.overlaps, list(intron=gr.hits[["hits"]]))
    message("annotate with 5'UTRs... ")
    five.utr = GenomicFeatures::fiveUTRsByTranscript(txdb, use.names=T)
    gr.hits = findOverlapsFeature(gr, five.utr, gene.ids, "utr5")
    gr = gr.hits[["gr"]]
    all.overlaps = c(all.overlaps, list(utr5=gr.hits[["hits"]]))
    if (!is.null(utr5.extend) && !is.na(utr5.extend) && utr5.extend > 0) {
        message(sprintf(
            "annotate as utr5* if %s upstream of 5'UTRs... ", utr5.extend
        ))
        five.utr.upstream = GenomicRanges::flank(
            five.utr, width=utr5.extend, start=T
        )
        gr.hits = findOverlapsFeature(gr, five.utr.upstream, gene.ids, "utr5*")
        gr = gr.hits[["gr"]]
        all.overlaps = c(all.overlaps, list(`utr5*`=gr.hits[["hits"]]))
    }
    message("annotate with 3'UTRs... ")
    three.utr = GenomicFeatures::threeUTRsByTranscript(txdb, use.names=T)
    gr.hits = findOverlapsFeature(gr, three.utr, gene.ids, "utr3")
    gr = gr.hits[["gr"]]
    all.overlaps = c(all.overlaps, list(utr3=gr.hits[["hits"]]))
    if (!is.null(utr3.extend) && !is.na(utr3.extend) && utr3.extend > 0) {
        message(sprintf(
            "annotate as utr3* if %s downstream of 3'UTRs... ", utr3.extend
        ))
        three.utr.upstream = GenomicRanges::flank(
            three.utr, width=utr3.extend, start=F
        )
        gr.hits = findOverlapsFeature(gr, three.utr.upstream, gene.ids, "utr3*")
        gr = gr.hits[["gr"]]
        all.overlaps = c(all.overlaps, list(`utr3*`=gr.hits[["hits"]]))
    }
    message("annotate with intergenic... ")
    if (length(gr) > 0) {
        gr$intergenic = T
        which.intergenic = !is.na(gr$exon) | !is.na(gr$intron)
        if (any(which.intergenic)) {
            gr[which.intergenic]$intergenic = F
        }
        if (("utr5*" %in% colnames(mcols(gr))) && any(!is.na(gr$"utr5*"))) {
            gr[!is.na(gr$"utr5*")]$intergenic = F
        }
        if (("utr3*" %in% colnames(mcols(gr))) && any(!is.na(gr$"utr3*"))) {
            gr[!is.na(gr$"utr3*")]$intergenic = F
        }
    }
    all.overlaps = plyr::ldply(all.overlaps, .id="feature")
    all.overlaps = data.table::as.data.table(all.overlaps)
    return(list(gr=gr, overlaps=all.overlaps))
}


prioritizeAnnotations = function(gr, feature.order, genenames)
### adapted from CLIPanalyze ###
{
    message(sprintf(
        "prioritize annotations in this order: %s ...",
        paste0(feature.order, collapse=", ")
    ))
    feature.diff = setdiff(feature.order, colnames(mcols(gr)))
    if (length(feature.diff) > 0)
        message(sprintf(
            "warning: these annotations are not available: %s",
            paste0(feature.diff, collapse=", ")
        ))
    feature.order = feature.order[feature.order %in% colnames(mcols(gr))]
    mcols(gr)[, "annot"] = as.character(NA)
    for (feature in feature.order) {
        assign.feature = (
            is.na(mcols(gr)[, "annot"]) & !is.na(mcols(gr)[, feature])
        )
        mcols(gr)[assign.feature, "annot"] = feature
        mcols(gr)[assign.feature, "main.gene"] = mcols(gr)[
            assign.feature, feature
        ]
    }
    # get strands based on primary gene annotations:
    strand.table = as.data.frame(genenames)
    row.names(strand.table) = make.names(genenames$gene_name, unique=T)
    assign.feature = (
        !is.na(mcols(gr)[, "annot"]) & mcols(gr)[, "annot"] != "intergenic"
    )
    GenomicRanges::strand(gr[assign.feature,]) = strand.table[
        gsub("-", ".", make.names(mcols(gr)[assign.feature, "main.gene"])),
        "strand"
    ]
    mcols(gr)[assign.feature, "main.gene.id"] = strand.table[
        gsub("-", ".", make.names(mcols(gr)[assign.feature, "main.gene"])),
        "gene_id"
    ]
    assign.feature = is.na(mcols(gr)[, "annot"])
    mcols(gr)[assign.feature, "annot"] = "intergenic"
    #necessary approximation for downstream functions:
    GenomicRanges::strand(gr[assign.feature,]) = "+"
    return(gr)
}


annotate.peaks = function(loaded.peaks, anno, annot.order)
### annotate peaks with `anno` data ###
{
    names(loaded.peaks) = c(paste0("peak", 1:length(loaded.peaks)))
    peaks.anno = annotateFeatures(
        loaded.peaks, txdb=anno$txdb, genenames=anno$genenames,
        utr5.extend=2000, utr3.extend=2000
    )
    names(peaks.anno) = c("peaks", "overlaps")
    peaks.anno$peaks = prioritizeAnnotations(
        peaks.anno$peaks, feature.order=annot.order, genenames=anno$genenames
    )
    return(peaks.anno)
}


import.peaks = function(peak.files, anno)
### import and reduce peak data from `peak.files` ###
{
    if (exists("loaded.peaks"))
        remove(loaded.peaks)
    for (file in peak.files) {
        if (exists("loaded.peaks")) {
            loaded.peaks = GenomicRanges::union(
                loaded.peaks, rtracklayer::import(file, format="BED")
            )
        } else {
            loaded.peaks = rtracklayer::import(file, format="BED")
        }
    }
    # merge overlapping peaks:
    loaded.peaks = GenomicRanges::reduce(loaded.peaks)
    # filter peaks by chromosomes (?):
    loaded.peaks = GenomeInfoDb::keepStandardChromosomes(
        loaded.peaks, pruning.mode="coarse"
    )
    # annotate macs2 peaks:
    peaks.anno = annotate.peaks(
        loaded.peaks, anno,
        c("utr3", "utr5", "exon", "intron", "utr3*", "utr5*")
    )
    remove(loaded.peaks)
    return(peaks.anno)
}


checkBamFileList = function(bamfiles, clean.names=T)
### adapted from CLIPanalyze ###
{
    if (is.character(bamfiles))
        bamfiles = Rsamtools::BamFileList(bamfiles)
    if (!is(bamfiles, "BamFileList"))
        stop("BamFileList required")
    if (!all(file.exists(BiocGenerics::path(bamfiles)))) {
        lost = !file.exists(BiocGenerics::path(bamfiles))
        stop(
            paste(BiocGenerics::path(bamfiles[lost]), collapse=","),
            " BAMs not found"
        )
    }
    if (clean.names) {
        nms = names(bamfiles)
        if (is.null(nms))
            nms = BiocGenerics::path(bamfiles)
        nms = gsub("\\.bam$", "", basename(nms))
        names(bamfiles) = make.unique(nms)
    }
    bamfiles
}


count.reads=function(peak.data, bamfiles, paired.end, extension, sample.names=NULL, stranded=0, nthreads=1)
### adapted from CLIPanalyze ###
{
    message("count reads in peaks...")
    bamfiles.filenames = bamfiles
    bamfiles = checkBamFileList(bamfiles)
    if (is.list(peak.data)) {
        peaks = peak.data$peaks
        if (!("peak.counts" %in% names(peak.data)))
            peak.data$peak.counts = NULL
        if (!("gene.counts.nopeaks" %in% names(peak.data)))
            peak.data$gene.counts.nopeaks = NULL
    } else {
        peaks = peak.data
        peak.data=list(peaks=peaks, peak.counts=NULL, gene.counts.nopeaks=NULL)
    }
    if (nthreads > 1) {
        message(sprintf("use %s threads in parallel", nthreads))
        BiocParallel::register(BiocParallel::MulticoreParam(workers=nthreads))
    }
    peaks.for.counts = peaks
    peak.df = data.frame(
        GeneID=names(peaks.for.counts),
        Chr=as.character(GenomicRanges::seqnames(peaks.for.counts)),
        Start=GenomicRanges::start(peaks.for.counts),
        End=GenomicRanges::end(peaks.for.counts),
        Strand=GenomicRanges::strand(peaks.for.counts)
    )
    peak.counts = Rsubread::featureCounts(
        bamfiles.filenames, annot.ext=peak.df, allowMultiOverlap=T,
        minFragLength=20, isPairedEnd=paired.end, readExtension3=extension,
        nthreads=nthreads
    )
    # strandSpecific = stranded:
    peak.counts = peak.counts$counts
    if (!is.null(sample.names)) {
        if (length(colnames(peak.counts)) == length(sample.names)) {
            colnames(peak.counts) <- sample.names
        } else {
            warning(
                "length of sample.names does not match colnames(peak.counts)"
            )
        }
    }
    peak.counts = DESeq2::DESeqDataSetFromMatrix(
        peak.counts, design=as.formula("~1"),
        colData=data.frame(row.names=colnames(peak.counts))
    )
    peak.data$peak.counts = peak.counts
    return(peak.data)
}


run.deseq2 = function(cnts, meta)
### run DESeq2 with generic metadata/annotation and return useful columns ###
{
    inf.dds = DESeq2::DESeqDataSetFromMatrix(
        countData=cnts, colData=meta, design=~Condition+IP+Condition:IP
    )
    inf.dds.LRT = DESeq2::DESeq(
        inf.dds, betaPrior=F, test="LRT", full=~Condition+IP+Condition:IP,
        reduced=~Condition+IP
    )
    inf.dds.res = DESeq2::results(inf.dds.LRT)
    results = as.data.frame(cbind(inf.dds.res$pvalue, inf.dds.res$padj))
    colnames(results) = c("deseq2.p", "deseq2.padj")
    return(results)
}


run.edger = function(cnts, meta)
### run edgeR with generic metadata/annotation and return useful columns ###
{
    #add count filter?
    er.design = model.matrix(~meta$Condition+meta$IP+meta$Condition*meta$IP)
    er.dgelist = edgeR::DGEList(counts=cnts, group=meta$Condition)
    er.dgelist = edgeR::estimateDisp(er.dgelist, design=er.design)
    er.fit = edgeR::glmFit(er.dgelist, er.design)
    er.lrt = edgeR::glmLRT(er.fit, coef=4)
    results = as.data.frame(
        cbind(er.lrt$table$PValue, p.adjust(er.lrt$table$PValue))
    )
    colnames(results) = c("edger.p", "edger.padj")
    return(results)
}


run.qnb = function(cnts, meta)
### run QNB with generic metadata/annotation and return useful columns ###
{
    meth1 = cnts[, which(meta$Condition == "treatment" & meta$IP == "IP")]
    meth2 = cnts[, which(meta$Condition != "treatment" & meta$IP == "IP")]
    unmeth1 = cnts[, which(meta$Condition == "treatment" & meta$IP == "input")]
    unmeth2 = cnts[, which(meta$Condition != "treatment" & meta$IP == "input")]
    qnb.result.sim = QNB::qnbtest(
        meth1, meth2, unmeth1, unmeth2, mode="per-condition"
    )
    results = as.data.frame(cbind(qnb.result.sim$pvalue, qnb.result.sim$padj))
    colnames(results) = c("qnb.p", "qnb.padj")
    return(results)
}


run.metdiff = function(cnts, meta)
### run MetDiff with generic metadata/annotation and return useful columns ###
{
    meth1 = as.data.frame(cnts[, which(meta$Condition == "treatment" & meta$IP == "IP")])
    meth2 = as.data.frame(cnts[, which(meta$Condition != "treatment" & meta$IP == "IP")])
    unmeth1 = as.data.frame(cnts[, which(meta$Condition == "treatment" & meta$IP == "input")])
    unmeth2 = as.data.frame(cnts[, which(meta$Condition != "treatment" & meta$IP == "input")])
    metdiff.result = diff.call.module(meth1, unmeth1, meth2, unmeth2)
    results = as.data.frame(cbind(
        metdiff.result$DIFF$pvalues, metdiff.result$DIFF$fdr
    ))
    colnames(results) = c("metdiff.p", "metdiff.padj")
    return(results)
}


run.deseq2.4l2fc = function(cnts, meta, label)
### additional DESeq2 step for genes l2FC and peak IP l2FC ###
{
    dds = DESeq2::DESeqDataSetFromMatrix(cnts, meta, formula(~Condition))
    dds$Condition = factor(dds$Condition, levels=c("control", "treatment"))
    gene.col2check = meta$Condition
    dds$Condition = droplevels(dds$Condition)
    gene.deseq = DESeq2::DESeq(dds)
    gene.deseq = DESeq2::results(gene.deseq)
    gene.results = gene.deseq[, c("log2FoldChange", "pvalue", "padj")]
    colnames(gene.results) = paste0(label, c(".l2fc", ".p", ".padj"))
    return(gene.results)
}


run.tools = function(results, peak.counts, meta, tool, input.bams, ip.bams, treated.input.bams, treated.ip.bams)
### dispatch data to DESeq2, edgeR, QNB ###
{
    tools = strsplit(tool, "")[[1]]
    full.results = T
    if ("d" %in% tools)
        results = cbind(results, run.deseq2(peak.counts, meta))
    if ("e" %in% tools)
        results = cbind(results, run.edger(peak.counts, meta))
    if ("q" %in% tools)
        results = cbind(results, run.qnb(peak.counts, meta))
    if ("m" %in% tools)
        results = cbind(results, run.metdiff(peak.counts, meta))
    return(results)
}


get.gene.counts = function(bamlist, gtffi, pe, extension, genenames)
### Alexa? ###
{
    gene.data = Rsubread::featureCounts(
        files=bamlist, GTF.featureType="exon", GTF.attrType="gene_id",
        annot.ext=gtffi, isGTFAnnotationFile=T,
        isPairedEnd=pe, readExtension3=extension
    )
    gene.counts = gene.data$counts
    conversion.table = as.data.frame(genenames)
    row.names(conversion.table) = make.names(genenames$gene_id, unique=T)
    row.names(gene.counts) = conversion.table[
        row.names(gene.counts), "gene_name"
    ]
    return(gene.counts)
}


interpret_input = function(input.bams, ip.bams, treated.input.bams, treated.ip.bams, peak.files, gtf, paired.end, readlen, fraglen)
{
    n.c = count_bams(input.bams, ip.bams)
    n.t = count_bams(treated.input.bams, treated.ip.bams)
    extension = fraglen - readlen
    # generate generic sample annotation:
    meta.data = make_metadata(n.c, n.t)
    # load gtf annotations and peaks:
    genenames = load_genenames(gtf)
    anno = load_anno(gtf, genenames)
    peaks = import.peaks(peak.files, anno)
    # count reads:
    all.bams = c(input.bams, ip.bams, treated.input.bams, treated.ip.bams)
    peaks = count.reads(peaks, all.bams, paired.end, extension)
    peak.counts = counts(peaks$peak.counts)
    # return multiple objects:
    results = peaks$peaks[, c("annot", "main.gene")]
    return(list(
        genenames=genenames,
        meta.data=meta.data,
        peaks=peaks,
        peak.counts=peak.counts,
        results=results,
        extension=extension
    ))
}


hands_on_deq = function(input.bams, ip.bams, treated.input.bams, treated.ip.bams, peak.files, gtf, outfi, tool="deq", paired.end=F, compare.gene=T, readlen=100, fraglen=100, nthreads=1)
### Main data dispatcher and collector ###
{
    c(genenames, meta.data, peaks, peak.counts, results, extension) %<-%
        interpret_input(
            input.bams, ip.bams, treated.input.bams, treated.ip.bams,
            peak.files, gtf, paired.end, readlen, fraglen
        )
    # run DESeq2, edgeR, and QNB to predict changes in m6A methylation:
    results = run.tools(
        results, peak.counts, meta.data, tool,
        input.bams, ip.bams, treated.input.bams, treated.ip.bams
    )
    peaks$peak.de = run.deseq2.4l2fc(
        peak.counts[, which(meta.data$IP=="IP")],
        meta.data[which(meta.data$IP=="IP"), ], "peak"
    )
    results$peak.l2fc = peaks$peak.de$peak.l2fc
    # calculate gene log2 fold change:
    if (compare.gene) {
        peaks$gene.counts = get.gene.counts(
            c(input.bams, treated.input.bams),
            gtf, paired.end, extension, genenames
        )
        sane_gene.counts = peaks$gene.counts
        rownames(sane_gene.counts) = make.names(rownames(sane_gene.counts))
        peaks$gene.de = run.deseq2.4l2fc(
            sane_gene.counts, meta.data[which(meta.data$IP=="input"), ], "gene"
        )
        results$gene.l2fc = peaks$gene.de[results$main.gene, ]$gene.l2fc
        results$gene.p = peaks$gene.de[results$main.gene, ]$gene.p
        results$gene.padj = peaks$gene.de[results$main.gene, ]$gene.padj
        results$diff.l2fc = results$peak.l2fc - results$gene.l2fc
    }
    # convert and write out results:
    results$start = results$start - 1
    colnames(peak.counts) = make.names(
        paste0(meta.data$Condition, "_", meta.data$IP), unique=T
    )
    write.table(
        peak.counts, gsub(".txt", ".counts.txt", outfi),
        quote=F, sep="\t", row.names=T, col.names=T
    )
    write.table(results, outfi, quote=F, sep="\t", row.names=F, col.names=T)
    return(results)
}
