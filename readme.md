Installation
============

```
git clone --recursive ${THIS_URL} meripseq2019
cd meripseq2019
conda env create --name meripseq2019 --file environment.yaml
conda activate meripseq2019
./Rwrap prototyping.r
```
